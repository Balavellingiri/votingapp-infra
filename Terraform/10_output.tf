output "project" {
  value = var.project
}

output "clustername" {
  value = var.clustername
}

output "zone" {
  value = var.zone
}

output "env" {
  value = var.env
}
