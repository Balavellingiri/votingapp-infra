##########
# global #
##########

variable "project" {
  description = "The google project ID"
  type        = string
  default     = ""
}

variable "env" {
  description = "Target infrastructure environment, e.g. 'ope', 'dev', 're7', 'tra'"
  type        = string
  default     = ""
}

variable "region" {
  description = "Common region for our resources"
  type        = string
  default     = ""
}

variable "zone" {
  description = "Common zone for our resources"
  type        = string
  default     = ""
}

variable "labels" {
  type    = map(string)
  default = {}
}

variable "clustername" {
  description = "Common zone for our resources"
  type        = string
  default     = ""
}
