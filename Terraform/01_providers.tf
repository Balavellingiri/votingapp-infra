provider "google" {
  project     = var.project
  region      = var.region
  credentials = file("GOOGLE_APPLICATION_CREDENTIALS.json")
  version     = "~> 3.0"
}

provider "google-beta" {
  project     = var.project
  region      = var.region
  credentials = file("GOOGLE_APPLICATION_CREDENTIALS.json")
  version     = "~> 3.0"
}
