terraform {
  backend "gcs" {
    bucket = "tf-bala4513-env-devtry"
    prefix = "terraform/state"
  }
}
