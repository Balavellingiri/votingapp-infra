# vim: noet:

.EXPORT_ALL_VARIABLES: GOOGLE_APPLICATION_CREDENTIALS
.PHONY: help

ENV ?= dev

TF_BUCKET_NAME := tf-bala4513-env-$(ENV)


help:  ## print this list
	@egrep -h '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

check-vars: ## ensure required env variables are defined
ifndef GOOGLE_APPLICATION_CREDENTIALS
	$(error GOOGLE_APPLICATION_CREDENTIALS is undefined)
endif
ifndef GCP_PROJECT_ID
	$(error GCP_PROJECT_ID is undefined)
endif
