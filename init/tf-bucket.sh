#!/usr/bin/env bash
set -e

gsutil ls "gs://${TF_BUCKET_NAME}" || gsutil mb -l us-central1 -c regional -p "${GCP_PROJECT_ID}" "gs://${TF_BUCKET_NAME}"

gsutil versioning set on "gs://${TF_BUCKET_NAME}"
