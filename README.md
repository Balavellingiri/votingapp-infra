# Voting-app Infra repository
This git project goal is to provide consistent infrastructure between all Voting environments (dev ,int , ope).  
Infrastructure changes should `always` be made via this repository, moreover `always` through the CI pipeline (not locally in your terminal).

# WARNING 
If you want to modify roles or access to this project, you should go to the corresponding root runners project in gitlab (rdev, rint and rope).
# Requirements
 - Renault gitlabee access on this project
 - Terraform 0.12 knowledge


# Repository structure
This repo is divided in 3 folders and 3 other files, each having a specific role.

## bin folder
Make file specifying variables needed to plan and apply our terraform stack.

## init folder
Combination of a makefile and a shell script to create remote state bucket the first time that the terraform is applied.

## terraform folder
The main and most important folder. This is where our infrastructure is described.

It also has its specific inside organization:
 - environments folder: specify each variable for the different environments.
 - 00_variables.tf: declare the variables (without giving them values), that will be specified inside the environments folder.
 - 01_providers.tf: which providers are needed by terraform to deploy our infrasctructure (mainly google and google-beta for us).
 - 02_backend.tf: where will be stored the state of our infra.
 - 1x_gcp_service.tf: infrastructure is divided between services and their interactions inside those files.
 - 99_outputs.tf: output values that we want to see at the end of our tf apply command.

## .gitignore file
If you've done some git, you know what it is doing.  
Helps managing your git history by ignoring file that you don't want to commit to this repository (secrets, local conf...)

## .gitlab-ci.yml file
Gitlab specific file where we manage the CI/CD pipeline (structure documented below).

## README.md file
This file. That should evolve with changes made to this repository. Each change should be documented here or in a release note file.


# CI/CD pipeline 

This project follows the gitflow principle. Meaning that there is a branch for each environment.
 - develop: for dev environment
 - integration: for int environment
 - master: for ope environment

Each change should always be made to `dev`, then `int` (if changes are validated in dev), then `ope` env (if changes are validated in int). 

## Commit syntax
Please respect a clear and concise template for your commits, so that they are readable for your current or future partners.  
I personnally use conventional commits syntax : https://www.conventionalcommits.org/en/v1.0.0-beta.2/

## Create a new feature or fixing existing content
 - `Always` start from the `develop` branch. 
 - Create a feature (or fix or whatever) branch, then start adding, fixing or removing content on this branch.
 - Once you're happy with what you've done, create a merge request against the `develop` branch.
 - Your code should be reviewed by a peer if possible.
 - Merge branch if your changes are validated and the pipeline is succesfull.

## Propagating changes to the INT or OPE environment
Changes for INT should always come from the `develop` branch and changes to OPE should always come from `integration` branch.  
The process is the same as for a new feature. Create a MR from the starting branch and against your target environment. (for instance `develop` -> `integration` for INT and `integration` -> `master` for OPE). 

